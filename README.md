## Telegram Bot on Ruby

# 🛠 Technology stack :
![Ruby](https://img.shields.io/badge/Ruby-CC342D?style=for-the-badge&logo=ruby&logoColor=white)

# 📄 About the project itself :

I wanted to implement something in ruby. I had the idea to write a bot since the days when I was engaged in python, and finally I got my hands on it
